# Double Submit Cookie Pattern

This project demostrate how to prevent Cross-site request forgery using double-submit-cookie-pattern

### Special npm command 
```
npm install
bower intall
```

#### If you yet not not install bower on your computer, run following command
```
npm install -g bower
```

If you get any error when bower intall command is executing in any unix distribution, then execute following command in terminal
```
sudo chown -R $USER:$GROUP ~/.npm
sudo chown -R $USER:$GROUP ~/.config
```
and run again bower install command

### Run application for execute below command in terminal/cmd
```
node server
```