app.config(function ($httpProvider, $locationProvider) {
        $httpProvider.defaults.withCredentials = true;
        $locationProvider.html5Mode(true).hashPrefix('');
})
