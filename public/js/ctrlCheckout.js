app.controller("ctrlCheckout", function($scope, $http, $cookies) {
  $scope.checkout = {};
  
  $scope.checkout.csrf_token = $cookies.get('csrf_cookie')
  $scope.message = "please enter location and submit!";
  $scope.form_hidden = false
  $scope.submitCheckout = function() {
    $http.post('/checkout', $scope.checkout).then(function (res) {
        $scope.message= res.data
    })
  };
});
