"use strict";

const express = require("express");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");

var app = express();
app.use("/public", express.static(__dirname + "/public"));
app.use("/bower_components", express.static(__dirname + "/bower_components"));
app.use(bodyParser.json());
app.use(cookieParser());

const session_id = "1234";
const csrf_cookie = "12345";
//Manage Login Part
app.get("/", function(req, res) {
  res.sendFile(__dirname + "/public/login.html");
});

app.post("/authentication", function(req, res) {
  let user = req.body;
  res.cookie("session_id", session_id, { maxAge: 900000, httpOnly: false });
  res.cookie("csrf_cookie", csrf_cookie, { maxAge: 900000, httpOnly: false });
  res.json(user);
});

app.get("/checkout", function(req, res) {
  res.sendFile(__dirname + "/public/checkout.html");
});

app.post("/checkout", function(req, res) {
  let checkout = req.body;
  let cookies = req.cookies;

  // double submit cookies check in here
  if (checkout.csrf_token == cookies.csrf_cookie) {
    res.json("successfully checkout!");
  } else {
    res.json("checkout cannot do now !");
  }
});

app.listen(3000, function(err) {
  if (err) console.log(err);
  else {
    console.log("Author : K.G. Sachintha Udara")
    console.log("ID : IT14141416")
    console.log("Email : kgsachintha@hotmail.com")
    console.log("Server successfully connected for Port No. 3000")
}
});
